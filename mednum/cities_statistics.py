from typing import Union

import numpy as np
import pandas as pd

from mednum.constants import DATA_MAPPING, DATA_FIELDS
from mednum.models import City, Departement, Epci

DECILE_SCORE = {
    0: -3,
    10: -2,
    20: -1,
    30: 0,
    40: 0,
    50: 0,
    60: 0,
    70: 1,
    80: 2,
    90: 3,
}


def compute_deciles():
    fields = list(DATA_MAPPING.values())
    values = City.objects.all().values(*list(fields))
    df = pd.DataFrame(values)
    deciles = range(10, 100, 10)

    percentiles = {}
    for field in fields:
        percentiles[field] = {}
        for decile in deciles:
            percentiles[field][decile] = np.nanpercentile(df[field], decile)

    return percentiles


class DecileValues(object):
    def __init__(self):
        self.deciles = None

    def get_deciles(self):
        if self.deciles is None:
            self.deciles = compute_deciles()
        return self.deciles


DECILE_VALUES = DecileValues()


def get_decile(value, field):
    """
    Return the decile for the (value, field) couple. The first decile
      (smallest values) is 0, the last decile is 90.
    """
    if value is None:
        return None

    for decile, decile_value in DECILE_VALUES.get_deciles()[field].items():
        if value < decile_value:
            return decile - 10
    else:
        return 90


def instance_scores(aggregate: Union[City, Epci, Departement], fields):
    """
    Returns fragility score for the departement/epci by averaging the scores
      for all cities in the departement/epci.
    """
    scores = {
        "name": aggregate.name,
        **{field: getattr(aggregate, f"{field}_score") for field in DATA_FIELDS},
    }
    filtered_scores = [getattr(aggregate, f"{field}_score") for field in fields]
    filtered_scores = [score for score in filtered_scores if score is not None]
    if len(filtered_scores):
        # individual scores are in [-3, 3]
        # total score is in [-10, 10]
        total_score = sum(filtered_scores) / len(filtered_scores) * 10 / 3
    else:
        total_score = None

    scores["total"] = total_score

    return scores
