from django.contrib.gis.geos import Polygon
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from mednum.cities_statistics import instance_scores
from mednum.models import City, Departement, Epci

CITIES_MIN_ZOOM_LEVEL = 7


def version(request):
    return HttpResponse("0.0.0")


def serialize_city(city: City) -> dict:
    return {
        "coordinates": city.geometry and city.geometry.coords,
        "type": "city",
        "name": city.name,
        "code": city.code,
        "department": city.departement,
    }


def serialize_departement(departement: Departement) -> dict:
    return {
        "coordinates": departement.geometry.coords,
        "type": "departement",
        "name": departement.name,
        "code": departement.code,
    }


def serialize_epci(epci: Epci) -> dict:
    return {
        "coordinates": epci.geometry.coords,
        "type": "epci",
        "name": epci.name,
        "code": epci.code,
    }


def geometries(request, division, north, east, south, west):
    north, east, south, west = float(north), float(east), float(south), float(west)
    bounding_box = Polygon(
        [
            (west, north),
            (east, north),
            (east, south),
            (west, south),
            (west, north),
        ]
    )
    if division == "departement":
        departements = Departement.objects.filter(geometry__intersects=bounding_box)
        geometries_list = [
            serialize_departement(departement) for departement in departements
        ]
    elif division == "epci":
        epcis = Epci.objects.filter(geometry__intersects=bounding_box)
        geometries_list = [serialize_departement(epci) for epci in epcis]
    elif division == "city":
        cities = City.objects.filter(geometry__intersects=bounding_box)
        geometries_list = [serialize_city(city) for city in cities]
    else:
        raise NotImplementedError(f"Division {division} not implemented")
    return JsonResponse({"division": division, "geometries": geometries_list})


def get_cities_with_epci(insee_code):
    city = City.objects.get(code=insee_code)
    cities = City.objects.filter(epci_code=city.epci_code)
    return cities


@csrf_exempt
def get_score(request, division: str):
    """Return the "fragility" score according the list of cities given."""
    filters = request.GET.getlist("filters[]", [])
    codes = request.GET.getlist("codes[]")
    if division == "city":
        model = City
    elif division == "epci":
        model = Epci
    elif division == "departement":
        model = Departement
    else:
        raise NotImplementedError(f"Division {division} not yet implemented")

    instances = model.objects.filter(code__in=codes)
    results = {}
    for instance in instances:
        results[instance.code] = {
            "score": instance_scores(instance, filters),
            "info": {
                "name": instance.name,
                "population": instance.population,
                "insee_code": instance.code,
                "mh_population": instance.mh_population if request.is_mh else None,
            },
        }
        if request.is_mh:
            results[instance.code]["info"]["mh_population"] = instance.mh_population
            results[instance.code]["info"]["center"] = instance.geometry.centroid.coords
    return JsonResponse(data=results, content_type="application/json")
