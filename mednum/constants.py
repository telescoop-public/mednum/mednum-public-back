HEADERS_MAPPING = {
    "CODE_INSEE": "code",
    "EPCI": "epci_code",
    "LIBEPCI": "epci_name",
    "LIBCOM": "name",
    "DEP": "departement",
    "POPULATION": "population",
}
DATA_MAPPING = {
    "TX_NON_THD": "no_thd_coverage_rate",
    "TX_PAUVRETE": "poverty_rate",
    "DIST_BIBLIOTHEQUE": "library_distance",
    "DIST_SERVICES_PUB": "public_service_distance",
    "TX_MENSEUL": "menseul_rate",
    "TX_FAMMONO": "fammono_rate",
    "TX_CHOMAGE": "unemployement_rate",
    "TX_ETRANGERS": "foreigners_rate",
    "TX_ALLO_50CAF": "social_assistance_rate",
    "TX_65ETPLUS": "older_65_rate",
    "TX_NSCOL15P": "nscol15p_rate",
    # elderly version
    "TX_75ETPLUS": "older_75_rate",
    "TX_PAUVRETE50PLUS": "poverty_rate_50_plus",
    "TX_PASPEUDIPLOM50PLUS": "nscol15p_rate_50_plus",
    "TX_MENSEUL50PLUS": "menseul_rate_50_plus",
    "TX_CHOMAGE50PLUS": "unemployement_rate_50_plus",
    "TX_ETRANGERS55PLUS": "foreigners_rate_55_plus",
}
DATA_FIELDS = list(DATA_MAPPING.values())
COLUMNS_MAPPING = dict(HEADERS_MAPPING, **DATA_MAPPING)
