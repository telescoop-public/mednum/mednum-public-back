import math
import re
from collections import defaultdict, Counter
from pathlib import Path

import pandas as pd
from Levenshtein import ratio as levenshtein_ratio
from django.core.management import BaseCommand
from tqdm import tqdm

from mednum.management.commands.utils import (
    import_all_cities_from_database,
    normalize_name,
)
from mednum.models import City
from mednum.settings import BASE_DIR


def to_int(v):
    try:
        return int(v)
    except ValueError:
        return None


def closest_city_name(cities, city_name, postal_code):
    max_ratio = 0
    found_insee_code = None
    found_city = None
    for insee_code, candidate_name in cities.normalized_name.iteritems():
        ratio = levenshtein_ratio(city_name, candidate_name)
        if ratio > max_ratio:
            max_ratio = ratio
            found_insee_code = insee_code
            found_city = candidate_name
    if max_ratio > 0.8:
        print(
            f'found match, score {max_ratio} "{city_name}" - "{found_city}", {postal_code} - {found_insee_code}'
        )
        return found_insee_code
    return None


def deal_with_arrondissement(cities, postal_code, city_name):
    metropoles = ["PARIS", "LYON", "MARSEILLE"]
    patterns = [
        r"CADEX (\d+)",
        r"CEDEX (\d+)",
        r"(\d+)ER ARRONDT",
        r"(\d+)EME ARRONDT",
        r"(\d+)ER ARRONDISSEMENT",
        r"(\d+)EME ARRONDISSEMENT",
        r"(\d+)$",
    ]
    for possible_city in metropoles:
        if city_name.startswith(possible_city + " "):
            city = possible_city
            break
    else:
        return None

    for pattern in patterns:
        match = re.findall(pattern, city_name)
        if match:
            arrondissement = match[0]
            break
    else:
        return None

    arrondissement = int(arrondissement)
    suffix = ""
    if arrondissement == 1:
        suffix = "R"

    try:
        row = cities[
            cities.normalized_name.str.contains(
                f"{city} {arrondissement}E{suffix} ARRONDISSEMENT"
            )
        ].iloc[0]
    except IndexError:
        raise ValueError(f"No arrondissement {arrondissement} in that city")

    print(
        f"found arrondissement for city {city_name} - {row['name']}, {postal_code} - {row.name}"
    )

    return row.name


def insee_code_from_postal_code_and_city_name(
    cities,
    manual_postal_code_to_insee_code,
    postal_to_insee_code,
    postal_code,
    city_name,
):
    # manual matching
    if postal_code in manual_postal_code_to_insee_code:
        print(
            "manual match, postal code",
            postal_code,
            city_name,
            manual_postal_code_to_insee_code[postal_code],
        )
        return manual_postal_code_to_insee_code[postal_code]

    # matching by postal code -> insee_code table, then finding the closest city
    if postal_code in postal_to_insee_code:
        insee_code = max(
            postal_to_insee_code[postal_code],
            key=lambda code: levenshtein_ratio(
                city_name, normalize_name(cities.loc[code]["name"])
            ),
        )
        return insee_code

    # matching for arrondissements
    possible_insee_code = deal_with_arrondissement(cities, postal_code, city_name)
    if possible_insee_code:
        return possible_insee_code

    # matching by city name where the name had CEDEX in it
    if " CEDEX" in city_name:
        city_name = city_name.split(" CEDEX")[0]
    sub_cities = cities[cities.normalized_name == city_name]

    # matching by city name if the city is close enough
    if sub_cities.shape[0]:
        return sub_cities.iloc[0].name
    insee_code = closest_city_name(cities, city_name, postal_code)

    # return insee code if one was found
    if insee_code:
        return insee_code
    raise ValueError()


def isnan(v):
    return isinstance(v, float) and math.isnan(v)


def postal_code_to_insee_code_translation(cities):
    cpos_insee = pd.read_csv(BASE_DIR / "data" / "laposte_hexasmal.csv", sep=";")
    postal_to_insee_code = defaultdict(set)
    for row in cpos_insee.itertuples():
        if row.Code_commune_INSEE not in cities.index:
            continue
        postal_to_insee_code[row.Code_postal].add(row.Code_commune_INSEE)
    return postal_to_insee_code


def is_excluded_postal_code(postal_code: int):
    """Exclude some postal codes, all Outre Mer except some territories."""
    postal_code = str(postal_code)
    for excluded_start in ["975", "976", "977", "978", "98"]:
        if postal_code.startswith(excluded_start):
            return True

    return False


RESULTS_CACHE = {}


def import_mh_data():
    if (
        not (BASE_DIR / "data" / "mh_data.xlsx").is_file()
        or not (BASE_DIR / "data" / "mh_data2.xlsx").is_file()
    ):
        raise FileNotFoundError(
            "mh_data.xlsx and mh_data2.xlsx must be in the data folder"
        )

    cities = import_all_cities_from_database()

    # create postal_to_insee_code translation
    postal_to_insee_code = postal_code_to_insee_code_translation(cities)
    manual_postal_code_to_insee_code = {
        6152: "06029",
        6156: "06029",
        6903: "06004",
        6902: "06004",
    }

    # load external data
    population_per_insee_code = Counter()
    errors = []
    results = []
    previous_error_count = 0
    for data_file in ["mh_data.xlsx", "mh_data2.xlsx"]:
        for sheet_index in range(6):
            try:
                df = pd.read_excel(
                    BASE_DIR / "data" / data_file, sheet_name=sheet_index
                )
            except ValueError:
                # all files don't have the same number of sheets
                continue

            print(f"{data_file}, sheet {sheet_index + 1} opened")

            for _, row in tqdm(
                df.iterrows(),
                total=df.shape[0],
                desc=f"handling {data_file} - {sheet_index + 1}/6",
            ):
                postal_code = row.CPOS
                city_name = row.LOCAL

                if is_excluded_postal_code(postal_code):
                    continue

                if (postal_code, city_name) in RESULTS_CACHE:
                    insee_code = RESULTS_CACHE[(postal_code, city_name)]
                else:
                    if isnan(city_name) or isnan(postal_code):
                        errors.append(row)
                        continue

                    try:
                        insee_code = insee_code_from_postal_code_and_city_name(
                            cities,
                            manual_postal_code_to_insee_code,
                            postal_to_insee_code,
                            postal_code,
                            city_name,
                        )
                    except ValueError:
                        print("error for", postal_code, city_name)
                        errors.append(row)
                        continue

                    RESULTS_CACHE[(postal_code, city_name)] = insee_code
                results.append(
                    [postal_code, city_name, insee_code, cities.loc[insee_code]["name"]]
                )
                population_per_insee_code[insee_code] += 1

            new_errors = len(errors) - previous_error_count
            previous_error_count = len(errors)
            print(
                f"finished handling {data_file}, sheet {sheet_index + 1} "
                f"with {new_errors} errors "
            )

    # save results to DB
    tot_count = 0
    for insee_code, population in tqdm(
        population_per_insee_code.items(),
        desc="saving results",
        total=len(population_per_insee_code),
    ):
        tot_count += population
        city = City.objects.get(code=insee_code)
        city.mh_population = population
        city.save()

    results_df = pd.DataFrame(results)
    results_df.columns = ["MH code postal", "MH ville", "code insee", "ville"]
    results_path = BASE_DIR / "data" / "attributions_mh.csv"
    results_df.to_csv(results_path)

    print("done :) ; attributions enregistrée à ", results_path)


class Command(BaseCommand):
    help = "Import MH population into the database."

    def handle(self, *args, **options):
        """Imports Malakoff Humanis data and creates a file to mark it imported."""
        import_mh_data()
        (Path(BASE_DIR) / "data" / "mh_data_imported").touch()
