import tqdm
from django.core.management import BaseCommand

from mednum.models import Epci, Departement


def update_aggregate_population():
    for epci in tqdm.tqdm(Epci.objects.all(), desc="EPCI"):
        epci.update_population()
    for dep in tqdm.tqdm(Departement.objects.all(), desc="departement"):
        dep.update_population()


class Command(BaseCommand):
    help = "Update EPCI and Departement aggregate populations."

    def handle(self, *args, **options):
        update_aggregate_population()
