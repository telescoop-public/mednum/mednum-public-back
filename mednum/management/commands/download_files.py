import os

import requests
from django.core.management import BaseCommand
from tqdm import tqdm
from mednum.settings import BASE_DIR


def download_files():
    url = [
        (
            "https://raw.githubusercontent.com/dataforgoodfr/batch8_mednum/master/data/processed/MERGE_data.csv",
            "mednum_data.csv",
        ),
        (
            "https://www.data.gouv.fr/fr/datasets/r/554590ab-ae62-40ac-8353-ee75162c05ee",
            "laposte_hexasmal.csv",
        ),
    ]

    for file_url in url:
        # check if file already exists to avoid download
        if os.path.isfile(BASE_DIR.__str__() + "/data/" + file_url[1]):
            continue
        # download file
        response = requests.get(file_url[0], stream=True)
        with open(os.path.join(BASE_DIR, "data", file_url[1]), "wb") as handle:
            for data in tqdm(response.iter_content()):
                handle.write(data)

    if not (BASE_DIR / "data" / "communes-avec-drom").is_dir():
        raise ValueError(
            "You need to download dataset "
            "https://www.data.gouv.fr/fr/datasets/contours-communes-france-administrative-format-admin-express-avec-arrondissements/ "
            "https://www.data.gouv.fr/fr/datasets/r/1f0276fc-fbdb-4535-a203-17944cd1cd33 "
            "and extract the contents in a subfolder of 'data', named "
            "'communes-avec-drom'"
        )


class Command(BaseCommand):
    help = "Download project data"

    def handle(self, *args, **options):
        download_files()
