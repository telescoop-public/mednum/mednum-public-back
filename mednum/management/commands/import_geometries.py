import os
from pathlib import Path
from typing import Union

import django
from django.core.management import BaseCommand
from django.conf import settings
import geopandas as gpd
from tqdm import tqdm
from django.contrib.gis.geos import (
    Polygon as DjangoPolygon,
    MultiPolygon as DjangoMultiPolygon,
    GEOSGeometry,
)
from shapely.geometry.polygon import Polygon as ShapelyPolygon
from shapely.geometry.multipolygon import MultiPolygon as ShapelyMultiPolygon

from mednum.management.commands.utils import (
    import_all_cities_from_database,
    DEPARTEMENT_CODE_TO_NAME,
)
from mednum.models import City, Departement, Epci
from mednum.settings import BASE_DIR


def add_geometry_to_instance(geometry, instance: Union[City, Departement, Epci]):
    if geometry is None:
        return
    if isinstance(geometry, django.contrib.gis.geos.polygon.Polygon):
        django_geometry = DjangoMultiPolygon(geometry, srid=geometry.srid)
    elif isinstance(geometry, django.contrib.gis.geos.collections.MultiPolygon):
        django_geometry = geometry
    else:
        if isinstance(geometry, ShapelyPolygon):
            data = [DjangoPolygon(list(geometry.exterior.coords))]
        elif isinstance(geometry, ShapelyMultiPolygon):
            data = [
                DjangoPolygon(list(polygon.exterior.coords)) for polygon in geometry
            ]
        else:
            raise ValueError(f"Unexpected geometry type ({type(geometry)})")
        django_geometry = DjangoMultiPolygon(data)
    instance.geometry = django_geometry
    instance.save()


# adding a third argument so that the signature matches that of
#  add_geometry_to_{departement,epci}
def add_geometry_to_city(geometry, code, _=None):
    try:
        add_geometry_to_instance(geometry, City.objects.get(code=code))
    except City.DoesNotExist:
        return


def add_geometry_to_departement(geometry, departement_code, departement_name):
    instance = Departement.objects.get_or_create(
        code=departement_code, defaults={"name": departement_name}
    )[0]
    add_geometry_to_instance(geometry, instance)


def add_geometry_to_epci(geometry, epci_code, epci_name):
    instance = Epci.objects.get_or_create(code=epci_code, defaults={"name": epci_name})[
        0
    ]
    add_geometry_to_instance(geometry, instance)


def save_instance_geometry_simplified_if_necessary(
    geometry, code, name, add_function, Model, min_simplification=None
):
    geometry_tolerances = [min_simplification, 1e-3, 3e-3, 5e-3, 8e-3, 1e-3, 3e-2, 5e-2]
    for tolerance in geometry_tolerances:
        if tolerance and tolerance < min_simplification:
            continue
        if tolerance is None:
            candidate_geometry = geometry
        else:
            candidate_geometry = geometry.simplify(tolerance=tolerance)
        add_function(candidate_geometry, code, name)
        try:
            if Model.objects.get(code=code).geometry is not None:
                break
        except Model.DoesNotExist:
            # a city might not exist if it is not in the Mednum database
            print(f"Did not exist {Model._meta.verbose_name} - {code}")
            break
    else:
        raise ValueError(
            f"Could not get geometry for {Model._meta.verbose_name} - {code}"
        )

    return tolerance


def import_geometries():  # noqa: C901
    min_simplification = {
        "city": 1e-3,
        "epci": 3e-3,
        "departement": 9e-3,
    }
    df = gpd.read_file(
        os.path.join(settings.BASE_DIR, "data", "communes-avec-drom", "COMMUNE.shp")
    )
    for row in tqdm(df.itertuples(), total=df.shape[0], desc="import cities"):
        geometry = GEOSGeometry(row.geometry.wkt, srid=3857)
        save_instance_geometry_simplified_if_necessary(
            geometry,
            row.INSEE_COM,
            None,
            add_geometry_to_city,
            City,
            min_simplification=min_simplification["city"],
        )

    # re-simplify geometries
    for instance in tqdm(City.objects.all(), desc="re-simplify cities"):
        try:
            save_instance_geometry_simplified_if_necessary(
                instance.geometry,
                instance.code,
                None,
                add_geometry_to_city,
                City,
                min_simplification=min_simplification["city"],
            )
        except ValueError:
            print("error for", instance.code)
            continue

    # get the epci and departement for each commune
    cities = import_all_cities_from_database()
    cities = cities.reset_index()[["code", "epci_code", "epci_name", "departement"]]

    df = df.merge(
        cities[["code", "epci_code", "epci_name", "departement"]],
        left_on="INSEE_COM",
        right_on="code",
        how="left",
    )

    epci = df[["geometry", "epci_code"]]
    epci = epci.dissolve(by="epci_code")
    epci_code_to_name = (
        cities.drop_duplicates("epci_name")[["epci_name", "epci_code"]]
        .set_index("epci_code")
        .to_dict()
    )["epci_name"]
    for row in tqdm(epci.itertuples(), desc="epci geometry", total=epci.shape[0]):
        code = row.Index
        if code not in epci_code_to_name:
            print(f"epci {code} missing")
            continue
        geometry = GEOSGeometry(row.geometry.wkt, srid=3857)
        save_instance_geometry_simplified_if_necessary(
            geometry,
            code,
            epci_code_to_name[code],
            add_geometry_to_epci,
            Epci,
            min_simplification=min_simplification["epci"],
        )

    # re-simplify geometries
    for instance in tqdm(Epci.objects.all(), desc="re-simplify EPCIs"):
        save_instance_geometry_simplified_if_necessary(
            instance.geometry,
            instance.code,
            epci_code_to_name[instance.code],
            add_geometry_to_epci,
            Epci,
            min_simplification=min_simplification["epci"],
        )
    for epci_instance in tqdm(Epci.objects.all(), desc="population epci"):
        epci_instance.update_population()

    departement = df[["geometry", "departement"]]
    departement = departement.dissolve(by="departement")
    for row in tqdm(
        departement.itertuples(), desc="dep geometry", total=departement.shape[0]
    ):
        code = row.Index
        geometry = GEOSGeometry(row.geometry.wkt, srid=3857)
        save_instance_geometry_simplified_if_necessary(
            geometry,
            code,
            DEPARTEMENT_CODE_TO_NAME[code],
            add_geometry_to_departement,
            Departement,
            min_simplification=min_simplification["departement"],
        )
    # re-simplify geometries
    for instance in tqdm(Departement.objects.all(), desc="re-simplify departements"):
        save_instance_geometry_simplified_if_necessary(
            instance.geometry,
            instance.code,
            DEPARTEMENT_CODE_TO_NAME[instance.code],
            add_geometry_to_departement,
            Departement,
            min_simplification=min_simplification["departement"],
        )
    for dep in Departement.objects.all():
        dep.update_population()


class Command(BaseCommand):
    help = "Import the geometries"

    def handle(self, *args, **options):
        """Import data and creates a file to mark it imported."""
        import_geometries()
        (Path(BASE_DIR) / "data" / "geometries_imported").touch()
