import os
from pathlib import Path

import tqdm
from django.core.management import BaseCommand
from django.conf import settings
import pandas as pd

from mednum.models import City
from mednum.constants import COLUMNS_MAPPING
from mednum.settings import BASE_DIR

marseille_insee_codes = [
    "13201",
    "13202",
    "13203",
    "13204",
    "13205",
    "13206",
    "13207",
    "13208",
    "13209",
    "13210",
    "13211",
    "13212",
    "13213",
    "13214",
    "13215",
    "13216",
]
lyon_insee_codes = [
    "69381",
    "69382",
    "69383",
    "69384",
    "69385",
    "69386",
    "69387",
    "69388",
    "69389",
]


def import_data():
    data_path = os.path.join(settings.BASE_DIR, "data", "indicators_per_city.xlsx")
    df = pd.read_excel(data_path)
    df = df.rename(columns=COLUMNS_MAPPING)
    # in database, NaNs are stored as None
    df = df.where(pd.notnull(df), None)

    # only keep relevant columns
    keep_columns = set(COLUMNS_MAPPING.values())
    df = df[[col for col in df.columns if col in keep_columns]]

    cities = []
    for city in tqdm.tqdm(df.itertuples(), total=df.shape[0]):
        city_data = {key: getattr(city, key) for key in keep_columns}
        if city_data["departement"] == "75":
            city_data["epci_name"] = "Métropole du Grand Paris"
            city_data["epci_code"] = "200054781"
        if city_data["code"] in marseille_insee_codes:
            city_data["epci_name"] = "Métropole d'Aix-Marseille-Provence"
            city_data["epci_code"] = "200054807"
        if city_data["code"] in lyon_insee_codes:
            city_data["epci_name"] = "Métropole de Lyon"
            city_data["epci_code"] = "200046977"
        cities.append(City(**city_data))
    City.objects.bulk_create(cities)


class Command(BaseCommand):
    help = "Import cities data"

    def handle(self, *args, **options):
        """Import data and creates a file to mark it imported."""
        import_data()
        (Path(BASE_DIR) / "data" / "data_imported").touch()
