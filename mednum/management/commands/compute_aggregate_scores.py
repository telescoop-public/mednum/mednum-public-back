import tqdm
from django.core.management import BaseCommand
from django.db import models

from mednum.cities_statistics import get_decile, DECILE_SCORE
from mednum.constants import DATA_FIELDS
from mednum.models import City, Epci, Departement


def compute_data_scores():
    """
    Computes the scores for Epci and Departement by averaging city scores
      and saves the results in the DB.

    Averaging is a weighted mean by the population. Population has to be
    computed per field as there can be missing values for some fields for
    some cities.
    """
    for city in tqdm.tqdm(
        City.objects.all(), total=City.objects.count(), desc="City scores"
    ):
        for field in DATA_FIELDS:
            value = getattr(city, field)
            if value is None:
                decile_value = None
            else:
                decile = get_decile(value, field)
                decile_value = DECILE_SCORE[decile]
            setattr(city, f"{field}_score", decile_value)
        city.save()

    for Model in [Epci, Departement]:
        for aggregate in tqdm.tqdm(
            Model.objects.all(),
            total=Model.objects.count(),
            desc=f"{Model._meta.verbose_name} scores",
        ):
            cities = aggregate.cities()
            for field in DATA_FIELDS:
                # when the city has no score for a field,
                # do not count that population for this field
                cities = cities.annotate(
                    **{
                        f"{field}_population": models.Case(
                            models.When(**{f"{field}_score__isnull": True, "then": 0}),
                            default=models.F("population"),
                            output_field=models.FloatField(),
                        )
                    }
                )
            # the sumprod for each field
            aggregation = {
                f"{field}_score_sumprod": models.Sum(
                    models.F(f"{field}_score") * models.F("population"),
                    output_field=models.FloatField(),
                )
                for field in DATA_FIELDS
            }
            # non-null population for each field
            aggregation = dict(
                aggregation,
                **{
                    f"{field}_population_sum": models.Sum(f"{field}_population")
                    for field in DATA_FIELDS
                },
            )
            data = cities.aggregate(**aggregation)

            # save results in DB
            for field in DATA_FIELDS:
                value = (
                    data[f"{field}_score_sumprod"]
                    and data[f"{field}_score_sumprod"] / data[f"{field}_population_sum"]
                )
                setattr(aggregate, f"{field}_score", value)
            aggregate.save()


class Command(BaseCommand):
    help = "Compute data scores"

    def handle(self, *args, **options):
        compute_data_scores()
