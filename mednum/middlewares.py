from mednum import settings


class MalakoffHumanisMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        is_mh = "malakoff-humanis" in request.headers["Host"]
        if settings.FORCE_MALAKOFF_HUMANIS:
            is_mh = True
        request.is_mh = is_mh
        response = self.get_response(request)
        return response
