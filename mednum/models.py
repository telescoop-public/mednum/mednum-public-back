from django.contrib.gis.db import models


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields.

    """

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateField(auto_now=True)

    class Meta:
        abstract = True


class City(TimeStampedModel):
    class Meta:
        verbose_name_plural = "cities"

    code = models.CharField(max_length=5, unique=True)
    epci_code = models.CharField(null=True, max_length=9)
    epci_name = models.CharField(null=True, max_length=46)
    name = models.CharField(null=False, max_length=39)
    population = models.PositiveIntegerField(null=True)
    departement = models.CharField(null=False, max_length=3)
    geometry = models.MultiPolygonField(
        null=True, blank=True, geography=True, srid=4326
    )
    mh_population = models.PositiveIntegerField(null=True, blank=True)

    # scores
    public_service_distance = models.FloatField(null=True)
    nscol15p_rate = models.FloatField(null=True)
    menseul_rate = models.FloatField(null=True)
    fammono_rate = models.FloatField(null=True)
    older_65_rate = models.FloatField(null=True)
    mobile_coverage = models.FloatField(null=True)
    no_thd_coverage_rate = models.FloatField(null=True)
    unemployement_rate = models.FloatField(null=True)
    foreigners_rate = models.FloatField(null=True)
    poverty_rate = models.FloatField(null=True)
    social_assistance_rate = models.FloatField(null=True)
    library_distance = models.FloatField(null=True)
    older_75_rate = models.FloatField(null=True)
    poverty_rate_50_plus = models.FloatField(null=True)
    nscol15p_rate_50_plus = models.FloatField(null=True)
    menseul_rate_50_plus = models.FloatField(null=True)
    unemployement_rate_50_plus = models.FloatField(null=True)
    foreigners_rate_55_plus = models.FloatField(null=True)

    # computed decile scores in [-3, 3] range
    public_service_distance_score = models.FloatField(null=True)
    nscol15p_rate_score = models.FloatField(null=True)
    menseul_rate_score = models.FloatField(null=True)
    fammono_rate_score = models.FloatField(null=True)
    older_65_rate_score = models.FloatField(null=True)
    mobile_coverage_score = models.FloatField(null=True)
    no_thd_coverage_rate_score = models.FloatField(null=True)
    unemployement_rate_score = models.FloatField(null=True)
    foreigners_rate_score = models.FloatField(null=True)
    poverty_rate_score = models.FloatField(null=True)
    social_assistance_rate_score = models.FloatField(null=True)
    library_distance_score = models.FloatField(null=True)
    older_75_rate_score = models.FloatField(null=True)
    poverty_rate_50_plus_score = models.FloatField(null=True)
    nscol15p_rate_50_plus_score = models.FloatField(null=True)
    menseul_rate_50_plus_score = models.FloatField(null=True)
    unemployement_rate_50_plus_score = models.FloatField(null=True)
    foreigners_rate_55_plus_score = models.FloatField(null=True)

    def __str__(self):
        return f"{self.name} (code {self.code})"


class Epci(TimeStampedModel):
    code = models.CharField(max_length=9, unique=True)
    name = models.CharField(max_length=70, null=True)
    geometry = models.MultiPolygonField(null=True, blank=True, geography=True)
    population = models.PositiveIntegerField(null=True)
    mh_population = models.PositiveIntegerField(null=True, blank=True)

    # computed decile scores
    public_service_distance_score = models.FloatField(null=True)
    nscol15p_rate_score = models.FloatField(null=True)
    menseul_rate_score = models.FloatField(null=True)
    fammono_rate_score = models.FloatField(null=True)
    older_65_rate_score = models.FloatField(null=True)
    mobile_coverage_score = models.FloatField(null=True)
    no_thd_coverage_rate_score = models.FloatField(null=True)
    unemployement_rate_score = models.FloatField(null=True)
    foreigners_rate_score = models.FloatField(null=True)
    poverty_rate_score = models.FloatField(null=True)
    social_assistance_rate_score = models.FloatField(null=True)
    library_distance_score = models.FloatField(null=True)
    older_75_rate_score = models.FloatField(null=True)
    poverty_rate_50_plus_score = models.FloatField(null=True)
    nscol15p_rate_50_plus_score = models.FloatField(null=True)
    menseul_rate_50_plus_score = models.FloatField(null=True)
    unemployement_rate_50_plus_score = models.FloatField(null=True)
    foreigners_rate_55_plus_score = models.FloatField(null=True)

    def update_population(self):
        self.population = City.objects.filter(epci_code=self.code).aggregate(
            models.Sum("population")
        )["population__sum"]
        self.mh_population = City.objects.filter(epci_code=self.code).aggregate(
            models.Sum("mh_population")
        )["mh_population__sum"]
        self.save()

    def compute_scores(self):
        pass

    def cities(self):
        return City.objects.filter(epci_code=self.code)


class Departement(TimeStampedModel):
    code = models.CharField(max_length=2, unique=True)
    name = models.CharField(max_length=23)
    geometry = models.MultiPolygonField(null=True, blank=True, geography=True)
    population = models.PositiveIntegerField(null=True)
    mh_population = models.PositiveIntegerField(null=True, blank=True)

    # computed decile scores
    public_service_distance_score = models.FloatField(null=True)
    nscol15p_rate_score = models.FloatField(null=True)
    menseul_rate_score = models.FloatField(null=True)
    fammono_rate_score = models.FloatField(null=True)
    older_65_rate_score = models.FloatField(null=True)
    mobile_coverage_score = models.FloatField(null=True)
    no_thd_coverage_rate_score = models.FloatField(null=True)
    unemployement_rate_score = models.FloatField(null=True)
    foreigners_rate_score = models.FloatField(null=True)
    poverty_rate_score = models.FloatField(null=True)
    social_assistance_rate_score = models.FloatField(null=True)
    library_distance_score = models.FloatField(null=True)
    older_75_rate_score = models.FloatField(null=True)
    poverty_rate_50_plus_score = models.FloatField(null=True)
    nscol15p_rate_50_plus_score = models.FloatField(null=True)
    menseul_rate_50_plus_score = models.FloatField(null=True)
    unemployement_rate_50_plus_score = models.FloatField(null=True)
    foreigners_rate_55_plus_score = models.FloatField(null=True)

    def __str__(self):
        return f"{self.name} ({self.code})"

    def update_population(self):
        self.population = City.objects.filter(departement=self.code).aggregate(
            models.Sum("population")
        )["population__sum"]
        self.mh_population = City.objects.filter(departement=self.code).aggregate(
            models.Sum("mh_population")
        )["mh_population__sum"]
        self.save()

    def cities(self):
        return City.objects.filter(departement=self.code)
