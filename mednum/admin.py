from django.contrib import admin
from django.contrib.auth.models import User, Group

from mednum.models import City, Departement

admin.site.unregister(User)
admin.site.unregister(Group)

admin.site.site_title = "Mednum - Carte fragilité numérique"
admin.site.site_header = "Mednum - Administration"
admin.site.index_title = "Mednum - Titre"


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    search_fields = ("code", "name")
    list_display = (
        "name",
        "code",
        "departement",
        "mh_population",
    )


@admin.register(Departement)
class DepartementAdmin(admin.ModelAdmin):
    search_fields = ("code", "name")
    list_display = (
        "code",
        "name",
    )
