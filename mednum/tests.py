from django.test import TestCase

from mednum.cities_statistics import instance_scores
from mednum.constants import DATA_FIELDS
from mednum.management.commands.compute_aggregate_scores import compute_data_scores
from mednum.models import City


class ScoringTestCase(TestCase):
    def setUp(self):
        self.best_city = City.objects.create(
            code="00001", name="best", departement="01"
        )
        self.average_city = City.objects.create(
            code="00002", name="average", departement="02"
        )
        self.worst_city = City.objects.create(
            code="00003", name="worst", departement="03"
        )
        for field in DATA_FIELDS:
            setattr(self.best_city, field, 0)
            setattr(self.average_city, field, 0.5)
            setattr(self.worst_city, field, 1)

        compute_data_scores()

        self.best_city.save()
        self.average_city.save()
        self.worst_city.save()

    def test_scoring(self):
        best_city_score = instance_scores(self.best_city, DATA_FIELDS)["total"]
        average_city_score = instance_scores(self.average_city, DATA_FIELDS)["total"]
        worst_city_score = instance_scores(self.worst_city, DATA_FIELDS)["total"]
        self.assertEqual(best_city_score, -10)
        self.assertEqual(average_city_score, 0)
        self.assertEqual(worst_city_score, 10)
