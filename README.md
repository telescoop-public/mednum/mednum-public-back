# Mednum Backend

## Data required

In the data directory, these files must be present. The last three can be downloaded
[here](https://github.com/gregoiredavid/france-geojson).

- `merge_data.csv`, [given by Mednum](https://github.com/dataforgoodfr/batch8_mednum/blob/master/data/processed/MERGE_data.csv)
- `communes.geojson`, the geometry of all cities in France
- `departements.geojson`, the geometry of all departements in France

And the arrondissements (downloaded files must be renamed):

- `arrondissements-paris.geojson` from [here](https://www.data.gouv.fr/fr/datasets/arrondissements-1/)
- `arrondissements-lyon.geojson` from [here](https://www.data.gouv.fr/fr/datasets/arrondissements-de-lyon/)
- `arrondissements-marseille.geojson` downloaded as kml from
[here](https://www.data.gouv.fr/fr/datasets/arrondissements-de-marseille-nd/)
and converted using [this online tool](https://mygeodata.cloud/converter/kml-to-geojson)


## Install requirements

Requires Gdal and Spatialite.

On Linux, install them with:

- gdal for geographic queries: sudo apt-get install gdal-bin
- sudo apt-get install libsqlite3-dev libproj-dev 
- sudo apt-get install libspatialite-dev spatialite-bin libsqlite3-mod-spatialite

## MacOs

- gdal for geographic queries : brew install gdal

## Import data

When you have the required data, run the following import management commands :

```bash
python manage.py import_data
python manage.py import_geometries
```
